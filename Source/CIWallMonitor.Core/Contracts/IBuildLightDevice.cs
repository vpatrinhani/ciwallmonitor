﻿using System.Collections.Generic;
using CIWallMonitor.Core.Models.CI;

namespace CIWallMonitor.Core.Contracts
{
    public interface IBuildLightDevice
    {
        void TriggerBuildBroken();
        void TriggerBuildOK();
        void TriggerBuildInProgress();
        void TurnOff();
    }
}