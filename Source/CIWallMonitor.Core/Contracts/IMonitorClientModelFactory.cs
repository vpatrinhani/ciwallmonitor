﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Core.Contracts
{
    public interface IMonitorClientModelFactory
    {
        TModel CreateModel<TModel>() where TModel : ModelBase;
    }
}
