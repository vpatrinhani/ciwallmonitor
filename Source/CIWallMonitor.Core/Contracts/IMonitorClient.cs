﻿using System.Collections.Generic;
using CIWallMonitor.Core.Models.CI;

namespace CIWallMonitor.Core.Contracts
{
    public interface IMonitorClient
    {
        IMonitorClientModelFactory ModelFactory { get; }

        List<ProjectModel> GetProjects();
        ProjectModel GetProject(string projectId);
        List<BuildTypeModel> GetBuildTypes();
        List<BuildModel> GetBuildsByBuildTypeID(string buildTypeID, int maxRescordsCount, bool getDetails = false);
        BuildModel GetBuild(string buildId);
        List<ChangeModel> GetChangesForBuildID(string buildId, int maxRescordsCount = 10);
        ChangeModel GetChange(string changeId);
        List<RunningBuildModel> GetRunningBuilds();
    }
}