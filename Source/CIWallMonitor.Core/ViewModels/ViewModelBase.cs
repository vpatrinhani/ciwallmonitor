﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Threading;
using Microsoft.Practices.Prism.ViewModel;

namespace CIWallMonitor.Core.ViewModels
{
    public class ViewModelBase : NotificationObject
    {
        #region Members

        private bool m_IsBusy;

        #endregion Members

        #region Properties

        public bool IsBusy
        {
            get { return m_IsBusy; }
            set
            {
                m_IsBusy = value;

                RaisePropertyChanged(() => IsBusy);
            }
        }

        #endregion Properties

        #region Methods

        protected void SendAsyncUIContext(Action act, bool isAsync = false, AsyncOperation asyncOper = null)
        {
            if (asyncOper == null)
            {
                asyncOper = GlobalContext.UIAsyncOperation;
            }

            if (isAsync)
            {
                asyncOper.SynchronizationContext.Send((o) => act(), null);
            }
            else
            {
                act();
            }
        }

        protected void RunOnDispatcher(Action action)
        {
            Dispatcher.CurrentDispatcher.BeginInvoke(action);
        }

        #endregion
    }
}
