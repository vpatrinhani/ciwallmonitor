﻿using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Core.Models.CI
{
    public class ChangeModel : ModelBase
    {
        #region Members

        private string m_Id;
        private string m_Date;
        private string m_UserName;
        
        #endregion Members

        #region Properties

        public string Id
        {
            get { return m_Id; }
            set
            {
                m_Id = value;

                RaisePropertyChanged(() => Id);
            }
        }

        public string Date
        {
            get { return m_Date; }
            set
            {
                m_Date = value;

                RaisePropertyChanged(() => Date);
            }
        }

        public string UserName
        {
            get { return m_UserName; }
            set
            {
                m_UserName = value;

                RaisePropertyChanged(() => UserName);
            }
        }

        #endregion Properties
    }
}