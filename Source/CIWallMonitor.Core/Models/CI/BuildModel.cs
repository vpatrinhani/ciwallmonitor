﻿using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Core.Models.CI
{
    public class BuildModel : ModelBase
    {
        #region Members

        private string m_Id;
        private string m_Number;
        private BuildStatus m_Status;
        private string m_StatusText;
        private ProjectModel m_BuildType;
        private string m_BuildTypeId;
        private string m_BuiltTypeName;
        
        #endregion Members

        #region Properties

        public string Id
        {
            get { return m_Id; }
            set
            {
                m_Id = value;

                RaisePropertyChanged(() => Id);
            }
        }

        public string Number
        {
            get { return m_Number; }
            set
            {
                m_Number = value;

                RaisePropertyChanged(() => Number);
            }
        }

        public BuildStatus Status
        {
            get { return m_Status; }
            set
            {
                m_Status = value;

                RaisePropertyChanged(() => Status);
            }
        }

        public string StatusText
        {
            get { return m_StatusText; }
            set
            {
                m_StatusText = value;

                RaisePropertyChanged(() => StatusText);
            }
        }

        public string BuildTypeId
        {
            get { return m_BuildTypeId; }
            set
            {
                m_BuildTypeId = value;

                RaisePropertyChanged(() => BuildTypeId);
            }
        }

        public string BuiltTypeName
        {
            get { return m_BuiltTypeName; }
            set
            {
                m_BuiltTypeName = value;

                RaisePropertyChanged(() => BuiltTypeName);
            }
        }

        public ProjectModel BuildType
        {
            get { return m_BuildType; }
            set
            {
                m_BuildType = value;

                RaisePropertyChanged(() => BuildType);
            }
        }

        #endregion Properties
    }
}