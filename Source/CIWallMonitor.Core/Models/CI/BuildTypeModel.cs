﻿using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Core.Models.CI
{
    public class BuildTypeModel : ModelBase
    {
        private string m_Id;
        private string m_Name;
        private ProjectModel m_Project;
        private string m_ProjectId;
        private string m_ProjectName;

        public string Id
        {
            get { return m_Id; }
            set
            {
                m_Id = value;

                RaisePropertyChanged(() => Id);
            }
        }

        public string Name
        {
            get { return m_Name; }
            set
            {
                m_Name = value;

                RaisePropertyChanged(() => Name);
            }
        }

        public string ProjectId
        {
            get { return m_ProjectId; }
            set
            {
                m_ProjectId = value;

                RaisePropertyChanged(() => ProjectId);
            }
        }

        public string ProjectName
        {
            get { return m_ProjectName; }
            set
            {
                m_ProjectName = value;

                RaisePropertyChanged(() => ProjectName);
            }
        }

        public ProjectModel Project
        {
            get { return m_Project; }
            set
            {
                m_Project = value;

                RaisePropertyChanged(() => Project);
            }
        }
    }
}