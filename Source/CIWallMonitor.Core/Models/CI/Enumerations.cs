﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CIWallMonitor.Core.Models.CI
{
    public enum BuildStatus
    {
        None,
        Success,
        Failure
    }
}
