﻿using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Core.Models.CI
{
    public class RunningBuildModel : ModelBase
    {
        #region Members

        private string m_BuildId;
        private string m_BuildTypeId;
        private string m_Text;
        private string m_RemainingTime;
        private string m_ElapsedTime;
        private int m_CompletedPercent;

        #endregion Members

        #region Properties

        public string BuildId
        {
            get { return m_BuildId; }
            set
            {
                m_BuildId = value;

                RaisePropertyChanged(() => BuildId);
            }
        }

        public string BuildTypeId
        {
            get { return m_BuildTypeId; }
            set
            {
                m_BuildTypeId = value;

                RaisePropertyChanged(() => BuildTypeId);
            }
        }

        public string Text
        {
            get { return m_Text; }
            set
            {
                m_Text = value;
                this.RaisePropertyChanged(() => Text);
            }
        }

        public string RemainingTime
        {
            get { return m_RemainingTime; }
            set
            {
                m_RemainingTime = value;
                this.RaisePropertyChanged(() => RemainingTime);
            }
        }

        public string ElapsedTime
        {
            get { return m_ElapsedTime; }
            set
            {
                m_ElapsedTime = value;
                this.RaisePropertyChanged(() => ElapsedTime);
            }
        }

        public int CompletedPercent
        {
            get { return m_CompletedPercent; }
            set
            {
                m_CompletedPercent = value;

                this.RaisePropertyChanged(() => CompletedPercent);
            }
        }

        #endregion Properties
    }
}