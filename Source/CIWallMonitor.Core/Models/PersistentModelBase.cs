﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.Prism.ViewModel;

namespace CIWallMonitor.Core.Models
{
    public class PersistentModelBase : ModelBase
    {
        public int Id { get; set; }
    }
}
