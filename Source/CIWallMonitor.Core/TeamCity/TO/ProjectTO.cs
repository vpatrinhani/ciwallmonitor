﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace CIWallMonitor.Core.TeamCity.TO
{
    [JsonObject]
    public class ProjectTO
    {
        [JsonProperty]
        public string id { get; set; }
        [JsonProperty]
        public string name { get; set; }

        [JsonProperty]
        public BuildTypeCollectionTO buildTypes { get; set; }
    }
}