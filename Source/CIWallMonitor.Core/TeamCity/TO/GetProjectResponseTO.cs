﻿using System.Collections.Generic;
using System.Xml.Serialization;
using CIWallMonitor.Core.Models;
using CIWallMonitor.Core.TeamCity.Models;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace CIWallMonitor.Core.TeamCity.TO
{
    public class GetProjectResponseTO
    {
        public ProjectTO project { get; set; }
    }
}