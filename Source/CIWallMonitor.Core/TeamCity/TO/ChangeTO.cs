﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CIWallMonitor.Core.TeamCity.TO
{
    [JsonObject]
    public class ChangeTO
    {
        [JsonProperty]
        public string id { get; set; }
        [JsonProperty]
        public string date { get; set; }
        [JsonProperty]
        public string username { get; set; }
        [JsonProperty]
        public string href { get; set; }
    }

    [JsonObject]
    public class ChangeTOCollection
    {
        [JsonProperty("change")]
        public IEnumerable<ChangeTO> change { get; set; }
    }
}