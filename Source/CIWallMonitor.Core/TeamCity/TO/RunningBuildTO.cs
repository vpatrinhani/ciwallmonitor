﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Newtonsoft.Json;

namespace CIWallMonitor.Core.TeamCity.TO
{
    public class RunningBuildTO
    {
        public string buildId { get; set; }
        public string buildTypeId { get; set; }
        public string text { get; set; }
        public bool successful { get; set; }
        public bool buildTypeSuccessful { get; set; }
        public string remainingTime { get; set; }
        public string exceededEstimatedDurationTime { get; set; }
        public string elapsedTime { get; set; }
        public int completedPercent { get; set; }
        public bool hasArtifacts { get; set; }
    }
}
