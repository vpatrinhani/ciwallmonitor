﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace CIWallMonitor.Core.TeamCity.TO
{
    public class GetRunningBuildsResponseTO
    {
        public List<RunningBuildTO> build { get; set; }
    }
}
