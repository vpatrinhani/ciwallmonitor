﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CIWallMonitor.Core.TeamCity.TO
{
    [JsonObject]
    public class BuildTypeTO
    {
        [JsonProperty]
        public string id { get; set; }
        [JsonProperty]
        public string name { get; set; }
        [JsonProperty]
        public string projectId { get; set; }
        [JsonProperty]
        public string projectName { get; set; }
    }

    [JsonObject]
    public class BuildTypeCollectionTO
    {
        [JsonProperty("buildType")]
        public IEnumerable<BuildTypeTO> buildType { get; set; }
    }
}