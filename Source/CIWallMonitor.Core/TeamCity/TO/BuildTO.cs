﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace CIWallMonitor.Core.TeamCity.TO
{
    [JsonObject]
    public class BuildTO
    {
        [JsonProperty]
        public string id { get; set; }
        [JsonProperty]
        public string number { get; set; }
        [JsonProperty]
        public string status { get; set; }
        [JsonProperty]
        public string statusText { get; set; }
        [JsonProperty]
        public string startDate { get; set; }
        [JsonProperty]
        public string finishDate { get; set; }
        [JsonProperty]
        public string href { get; set; }
        [JsonProperty]
        public string buildTypeId { get; set; }
        [JsonProperty]
        public string buildTypeName { get; set; }
    }

    [JsonObject]
    public class BuildTOCollectionTO
    {
        [JsonProperty("build")]
        public IEnumerable<BuildTO> build { get; set; }
    }
}