﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using CIWallMonitor.Core.Models.CI;
using CIWallMonitor.Core.TeamCity.Models.CI;
using CIWallMonitor.Core.TeamCity.TO;
using System.Xml.Linq;

namespace CIWallMonitor.Core.TeamCity
{
    public static class ConfigurationMappers
    {
        public static void Initialize()
        {
            Mapper.CreateMap<TeamCity.TO.ProjectTO, TeamCity.Models.CI.ProjectModel>()
                .ForMember(t => t.BuildTypes, opt => opt.MapFrom(s => s.buildTypes.buildType));

            Mapper.CreateMap<TeamCity.TO.BuildTypeTO, TeamCity.Models.CI.BuildTypeModel>();

            Mapper.CreateMap<TeamCity.TO.BuildTO, TeamCity.Models.CI.BuildModel>()
                .ForMember(t => t.Status, o => o.ResolveUsing((source) =>
                {
                    var _status = BuildStatus.Success;

                    switch (source.status)
                    {
                        case "SUCCESS":
                            {
                                _status = BuildStatus.Success;
                                break;
                            }
                        case "FAILURE":
                            {
                                _status = BuildStatus.Failure;
                                break;
                            }
                    }

                    return _status;
                }));

            Mapper.CreateMap<TeamCity.TO.ChangeTO, TeamCity.Models.CI.ChangeModel>();

            Mapper.CreateMap<XElement, TeamCity.TO.RunningBuildTO>()
                .ConvertUsing((source) =>
                {
                    if (source != null)
                    {
                        var _target = new TeamCity.TO.RunningBuildTO()
                        {
                            buildId = source.Attribute(XName.Get("buildId")).Value,
                            buildTypeId = source.Attribute(XName.Get("buildTypeId")).Value,
                            text = source.Attribute(XName.Get("text")).Value,
                            successful = Boolean.Parse(source.Attribute(XName.Get("successful")).Value),
                            buildTypeSuccessful = Boolean.Parse(source.Attribute(XName.Get("buildTypeSuccessful")).Value),
                            remainingTime = source.Attribute(XName.Get("remainingTime")).Value,
                            exceededEstimatedDurationTime = source.Attribute(XName.Get("exceededEstimatedDurationTime")).Value,
                            elapsedTime = source.Attribute(XName.Get("elapsedTime")).Value,
                            completedPercent = Int32.Parse(source.Attribute(XName.Get("completedPercent")).Value),
                            hasArtifacts = Boolean.Parse(source.Attribute(XName.Get("hasArtifacts")).Value),
                        };

                        return _target;
                    }
                    else
                    {
                        return null;
                    }
                });

            Mapper.CreateMap<TeamCity.TO.RunningBuildTO, TeamCity.Models.CI.RunningBuildModel>();
        }
    }
}
