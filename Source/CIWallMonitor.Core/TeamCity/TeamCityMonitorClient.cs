﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Xml.Serialization;
using AutoMapper;
using CIWallMonitor.Core.Configurations.Services;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.TeamCity.Models;
using CIWallMonitor.Core.TeamCity.Models.CI;
using CIWallMonitor.Core.TeamCity.TO;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.Unity;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Extensions;
using RestSharp.Serializers;

/*
 * http://<ipaddress>/app/rest/application.wadl
 * ->
 * http://<ipaddress>/app/rest/buildTypes
 * http://<ipaddress>/app/rest/buildTypes/id:bt2
 * http://<ipaddress>/app/rest/buildTypes/id:bt2/builds
 * http://<ipaddress>/app/rest/buildTypes/id:bt2/builds?count=4
 * http://<ipaddress>/app/rest/builds/id:4520
 * http://<ipaddress>/app/rest/changes?build=id:4520
 * http://<ipaddress>/app/rest/changes/id:2117
 * 
 * http://<ipaddress>/app/rest/builds/?locator=running:true
 * http://<ipaddress>/ajax.html?getRunningBuilds=1
 * 
<response>
    <build buildId="4554" buildTypeId="bt11" text="Repository sources transferred" successful="true" buildTypeSuccessful="false" remainingTime="3h:52m" exceededEstimatedDurationTime="< 1s" elapsedTime="3s" completedPercent="0" hasArtifacts="false"/>
</response>
 * 
 * http://<ipaddress>/serverStatistics.html
 */

namespace CIWallMonitor.Core.TeamCity
{
    /// <summary>
    /// http://<ipaddress>/app/rest/application.wadl
    /// </summary>
    public class TeamCityMonitorClient : IMonitorClient
    {
        #region Constants

        public const string DEFAULT_API_PATH = "/app/rest";

        #endregion

        #region Members

        private RestClient m_RestClient;
        private string m_ServerAddress;
        private int m_AddressPort;
        private string m_UserName;
        private string m_Password;
        private bool m_UseHttps;
        private IMonitorClientModelFactory m_ModelFactory;
        private object _lock = new object();

        #endregion Members

        #region Properties

        public IMonitorClientModelFactory ModelFactory
        {
            get { return m_ModelFactory; }
        }

        #endregion Properties

        #region Constructors

        [InjectionConstructor]
        public TeamCityMonitorClient([Dependency("MonitorConfiguration")] MonitorConfigurationService configurationService)
        {
            var _configurationModel = configurationService.GetMonitorConfiguration();

            m_ServerAddress = _configurationModel.ServerAddress;
            m_AddressPort = _configurationModel.AddressPort;
            m_UserName = _configurationModel.UserName;
            m_Password = _configurationModel.Password;
            m_UseHttps = _configurationModel.UseSSL;

            this.initialize();
        }

        public TeamCityMonitorClient(string serverAddress = "localhost", int addrresPort = 8080, bool useHttps = false, string username = null, string password = null)
        {
            m_ServerAddress = serverAddress;
            m_AddressPort = addrresPort;
            m_UserName = username;
            m_Password = password;
            m_UseHttps = useHttps;

            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void initialize()
        {
            m_ModelFactory = new TeamCityMonitorClientModelFactory();

            m_RestClient = new RestClient(
                String.Format("{0}://{1}:{2}",
                    ((m_UseHttps) ? "https" : "http"),
                    m_ServerAddress,
                    m_AddressPort
                )
            );

            m_RestClient.Timeout = Convert.ToInt32(TimeSpan.FromSeconds(15).TotalMilliseconds);

            if (!String.IsNullOrWhiteSpace(m_UserName))
            {
                m_RestClient.Authenticator = new HttpBasicAuthenticator(m_UserName, m_Password);
            }
        }

        private RestRequest createJSonRestRequest(string resource, bool isApi = true)
        {
            string _resourcePath = String.Format("{0}/{1}", DEFAULT_API_PATH, resource);

            if (!isApi)
            {
                _resourcePath = resource;
            }

            var _request = new RestRequest(_resourcePath);

            _request.Timeout = Convert.ToInt32(TimeSpan.FromSeconds(15).TotalMilliseconds);

            return _request;
        }

        private RestRequest createXmlRestRequest(string resource, bool isApi = true)
        {
            string _resourcePath = String.Format("{0}/{1}", DEFAULT_API_PATH, resource);

            if (!isApi)
            {
                _resourcePath = resource;
            }

            var _request = new RestRequest(_resourcePath);

            _request.Timeout = Convert.ToInt32(TimeSpan.FromSeconds(15).TotalMilliseconds);

            _request.AddHeader("Accept", "application/xml");
            _request.AddHeader("Content-Type", "application/xml");

            return _request;
        }

        #endregion Methods

        public List<Core.Models.CI.ProjectModel> GetProjects()
        {
            var _returnValue = new List<Core.Models.CI.ProjectModel>();

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("projects");

                    var _response = m_RestClient.Execute<GetProjectsResponseTO>(_request);

                    if (_response.Data != null)
                    {
                        _response.Data.project.ForEach(item => _returnValue.Add(Mapper.Map<ProjectModel>(item)));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public Core.Models.CI.ProjectModel GetProject(string projectId)
        {
            ProjectModel _returnValue = null;

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("projects/{id}");

                    _request.AddUrlSegment("id", projectId);

                    var _response = m_RestClient.Execute<ProjectTO>(_request);

                    var _obj = JsonConvert.DeserializeObject<ProjectTO>(_response.Content);

                    _returnValue = Mapper.Map<ProjectModel>(_obj);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public List<Core.Models.CI.BuildTypeModel> GetBuildTypes()
        {
            var _returnValue = new List<Core.Models.CI.BuildTypeModel>();

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("buildTypes");

                    var _response = m_RestClient.Execute<GetBuildTypesResponseTO>(_request);

                    if (_response.Data != null)
                    {
                        _response.Data.buildType.ForEach(item => _returnValue.Add(Mapper.Map<BuildTypeModel>(item)));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public List<Core.Models.CI.BuildModel> GetBuildsByBuildTypeID(string buildTypeID, int maxRescordsCount, bool getDetails = false)
        {
            var _returnValue = new List<Core.Models.CI.BuildModel>();

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("buildTypes/id:{id}/builds?count={count}");

                    _request.AddUrlSegment("id", buildTypeID);
                    _request.AddUrlSegment("count", maxRescordsCount.ToString());

                    var _response = m_RestClient.Execute<GetBuildsResponseTO>(_request);

                    if (_response.Data != null)
                    {
                        _response.Data.build.ForEach(item =>
                        {
                            if (getDetails)
                            {
                                var _modelDetails = this.GetBuild(item.id);

                                _returnValue.Add(_modelDetails);
                            }
                            else
                            {
                                _returnValue.Add(Mapper.Map<BuildModel>(item));
                            }
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public Core.Models.CI.BuildModel GetBuild(string buildId)
        {
            BuildModel _returnValue = null;

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("builds/id:{id}");

                    _request.AddUrlSegment("id", buildId);

                    var _response = m_RestClient.Execute<BuildTO>(_request);

                    var _obj = JsonConvert.DeserializeObject<BuildTO>(_response.Content);

                    _returnValue = Mapper.Map<BuildModel>(_obj);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public List<Core.Models.CI.ChangeModel> GetChangesForBuildID(string buildId, int maxRescordsCount = 0)
        {
            var _returnValue = new List<Core.Models.CI.ChangeModel>();

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("changes?build=id:{buildId}&count={count}");

                    _request.AddUrlSegment("buildId", buildId);

                    if (maxRescordsCount > 0)
                    {
                        _request.AddUrlSegment("count", maxRescordsCount.ToString());
                    }

                    var _response = m_RestClient.Execute<GetChangesForBuildIDResponseTO>(_request);

                    if (_response.Data != null)
                    {
                        _response.Data.change.ForEach(item =>
                        {
                            _returnValue.Add(Mapper.Map<ChangeModel>(item));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public Core.Models.CI.ChangeModel GetChange(string changeId)
        {
            ChangeModel _returnValue = null;

            try
            {
                lock (_lock)
                {
                    var _request = this.createJSonRestRequest("changes/id:{id}");

                    _request.AddUrlSegment("id", changeId);

                    var _response = m_RestClient.Execute<ChangeTO>(_request);

                    var _obj = JsonConvert.DeserializeObject<ChangeTO>(_response.Content);

                    _returnValue = Mapper.Map<ChangeModel>(_obj);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }

        public List<Core.Models.CI.RunningBuildModel> GetRunningBuilds()
        {
            var _returnValue = new List<Core.Models.CI.RunningBuildModel>();

            try
            {
                lock (_lock)
                {
                    var _request = this.createXmlRestRequest("ajax.html?getRunningBuilds=4", false);

                    var _response = m_RestClient.Execute<GetRunningBuildsResponseTO>(_request);

                    var _xDoc = XDocument.Parse(_response.Content);

                    if (_response.Content != null)
                    {
                        var _elements = _xDoc.Descendants(XName.Get("build"));

                        _elements.ForEach(item =>
                        {
                            var _runningBuildTO = Mapper.Map<TeamCity.TO.RunningBuildTO>(item);

                            _returnValue.Add(Mapper.Map<RunningBuildModel>(_runningBuildTO));
                        });
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return _returnValue;
        }
    }

    internal class TeamCityMonitorClientModelFactory : IMonitorClientModelFactory
    {
        public TModel CreateModel<TModel>() where TModel : Core.Models.ModelBase
        {
            TModel _returnModel = default(TModel);

            if (typeof(TModel) == typeof(Core.Models.CI.BuildModel))
            {
                var _model = new BuildModel();

                return (_model as TModel);
            }

            if (typeof(TModel) == typeof(Core.Models.CI.RunningBuildModel))
            {
                var _model = new RunningBuildModel();

                return (_model as TModel);
            }

            return _returnModel;
        }
    }

}
