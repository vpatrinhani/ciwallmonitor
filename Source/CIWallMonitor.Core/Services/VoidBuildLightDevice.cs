﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Services.HIDIOWINCS;

namespace CIWallMonitor.Core.Services
{
    public class VoidBuildLightDevice : IBuildLightDevice
    {
        public void TriggerBuildBroken()
        {
        }

        public void TriggerBuildOK()
        {
        }

        public void TriggerBuildInProgress()
        {
        }

        public void TurnOff()
        {
        }
    }
}
