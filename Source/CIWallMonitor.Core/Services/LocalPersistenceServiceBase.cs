﻿using System.IO;
using Lex.Db;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CIWallMonitor.Core.Services
{
    public class LocalPersistenceServiceBase : ServiceBase
    {
        #region Members

        private DbInstance m_DBInstance;

        #endregion Members

        #region Properties

        protected DbInstance DBInstance
        {
            get { return m_DBInstance; }
            private set { m_DBInstance = value; }
        }

        #endregion Properties

        #region Constructors

        public LocalPersistenceServiceBase(string localDBName)
        {
            this.initializeDBInstance(localDBName);
        }

        #endregion Constructors

        #region Methods

        private void initializeDBInstance(string localDBName)
        {
            //var _dbFolder = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, localDBName);

            this.DBInstance = new DbInstance(localDBName);   

            this.InitializeDBMap();

            this.DBInstance.Initialize();
        }

        protected virtual void InitializeDBMap()
        {
            
        }

        #endregion Methods
    }
}
