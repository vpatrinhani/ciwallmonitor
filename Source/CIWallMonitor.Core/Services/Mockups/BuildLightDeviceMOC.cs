﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CIWallMonitor.Core.Contracts;

namespace CIWallMonitor.Core.Services.Mockups
{
    public class BuildLightDeviceMOC : IBuildLightDevice
    {
        private CancellationTokenSource m_TriggerBuildBrokenCancellationToken;
        private CancellationTokenSource m_TriggerBuildOKCancellationToken;
        private CancellationTokenSource m_TriggerBuildInProgressCancellationToken;
        private Task m_TriggerBuildBrokenTask;
        private Task m_TriggerBuildOKTask;
        private Task m_TriggerBuildInProgressTask;

        public void TriggerBuildBroken()
        {
            m_TriggerBuildBrokenCancellationToken = new CancellationTokenSource();

            m_TriggerBuildBrokenTask = System.Threading.Tasks.Task.Run(() =>
            {
                while (!m_TriggerBuildBrokenCancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("BuildBroken ({0})", DateTime.Now);
                    Thread.Sleep(1000);
                }
            }, m_TriggerBuildBrokenCancellationToken.Token);
        }

        public void TriggerBuildOK()
        {
            m_TriggerBuildOKCancellationToken = new CancellationTokenSource();

            m_TriggerBuildOKTask = System.Threading.Tasks.Task.Run(() =>
            {
                for (int i = 0; i < 2; i++)
                {
                    Console.WriteLine("BuildOK ({0})", DateTime.Now);
                    Thread.Sleep(1000);
                }
            }, m_TriggerBuildOKCancellationToken.Token);
        }

        public void TriggerBuildInProgress()
        {
            m_TriggerBuildInProgressCancellationToken = new CancellationTokenSource();

            m_TriggerBuildInProgressTask = System.Threading.Tasks.Task.Run(() =>
            {
                while (!m_TriggerBuildInProgressCancellationToken.IsCancellationRequested)
                {
                    Console.WriteLine("InProgress ({0})", DateTime.Now);
                    Thread.Sleep(1000);
                }
            }, m_TriggerBuildInProgressCancellationToken.Token);
        }

        public void TurnOff()
        {
            if (m_TriggerBuildBrokenCancellationToken != null)
            {
                m_TriggerBuildBrokenCancellationToken.Cancel();
            }

            if (m_TriggerBuildOKCancellationToken != null)
            {
                m_TriggerBuildOKCancellationToken.Cancel();
            }

            if (m_TriggerBuildInProgressCancellationToken != null)
            {
                m_TriggerBuildInProgressCancellationToken.Cancel();
            }
        }
    }
}
