﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Services.HIDIOWINCS;

namespace CIWallMonitor.Core.Services
{
    public class DelcomBuildLightDevice : IBuildLightDevice, IDisposable
    {
        private bool m_BuildOKHIDDisplaying = false;
        private bool m_BuildInProgressHIDDisplaying;
        private bool m_BuildBrokenHIDDisplaying;

        private DelcomHID Delcom = new DelcomHID();   // declare the Delcom class
        private DelcomHID.HidTxPacketStruct TxCmd;
        Byte LED_GREEN_PIN, LED_RED_PIN, LED_BLUE_PIN;

        private CancellationTokenSource m_TriggerBuildBrokenCancellationToken;
        private CancellationTokenSource m_TriggerBuildOKCancellationToken;
        private CancellationTokenSource m_TriggerBuildInProgressCancellationToken;
        private Task m_TriggerBuildBrokenTask;
        private Task m_TriggerBuildOKTask;
        private Task m_TriggerBuildInProgressTask;

        public void TriggerBuildBroken()
        {
            lock (Delcom)
            {
                m_BuildOKHIDDisplaying = false;
                m_BuildInProgressHIDDisplaying = false;

                if (m_BuildBrokenHIDDisplaying) return;

                m_BuildBrokenHIDDisplaying = true;
            }

            m_TriggerBuildBrokenCancellationToken = new CancellationTokenSource();

            m_TriggerBuildBrokenTask = System.Threading.Tasks.Task.Run(() =>
            {
                lock (Delcom)
                {
                    this.resetHID();

                    this.openHID();

                    try
                    {
                        this.lightRedFlash();
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        this.closeHID();
                        m_BuildBrokenHIDDisplaying = false;
                    }
                }
            }, m_TriggerBuildBrokenCancellationToken.Token);
        }

        public void TriggerBuildOK()
        {
            lock (Delcom)
            {
                m_BuildBrokenHIDDisplaying = false;
                m_BuildInProgressHIDDisplaying = false;

                if (m_BuildOKHIDDisplaying) return;

                m_BuildOKHIDDisplaying = true;
            }

            m_TriggerBuildOKCancellationToken = new CancellationTokenSource();

            m_TriggerBuildOKTask = System.Threading.Tasks.Task.Run(() =>
            {
                lock (Delcom)
                {
                    this.resetHID();

                    this.openHID();

                    try
                    {
                        this.lightGreenFlash();
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        this.closeHID();

                        startActionAfter(TimeSpan.FromSeconds(4), () =>
                        {
                            lock (Delcom)
                            {
                                this.resetHID();
                            }
                        });
                    }
                }
            }, m_TriggerBuildOKCancellationToken.Token);
        }

        public void TriggerBuildInProgress()
        {
            lock (Delcom)
            {
                m_BuildBrokenHIDDisplaying = false;
                m_BuildOKHIDDisplaying = false;

                if (m_BuildInProgressHIDDisplaying) return;

                m_BuildInProgressHIDDisplaying = true;
            }

            m_TriggerBuildInProgressCancellationToken = new CancellationTokenSource();

            m_TriggerBuildInProgressTask = System.Threading.Tasks.Task.Run(() =>
            {
                lock (Delcom)
                {
                    this.resetHID();

                    this.openHID();

                    try
                    {
                        this.lightYellowFlash();
                    }
                    catch (Exception)
                    {
                    }
                    finally
                    {
                        this.closeHID();

                        startActionAfter(TimeSpan.FromSeconds(20), () =>
                        {
                            lock (Delcom)
                            {
                                this.resetHID();
                            }
                        });
                    }
                }
            }, m_TriggerBuildInProgressCancellationToken.Token);
        }

        public void TurnOff()
        {
            lock (Delcom)
            {
                    resetHID();
            }

            if (m_TriggerBuildBrokenCancellationToken != null)
            {
                m_TriggerBuildBrokenCancellationToken.Cancel();
            }

            if (m_TriggerBuildOKCancellationToken != null)
            {
                m_TriggerBuildOKCancellationToken.Cancel();
            }

            if (m_TriggerBuildInProgressCancellationToken != null)
            {
                m_TriggerBuildInProgressCancellationToken.Cancel();
            }
        }

        private void resetHID()
        {
            this.openHID();

            try
            {
                lightGreenOff();
                lightRedOff();
                lightYellowOff();
            }
            catch (Exception)
            {
            }
            finally
            {
                this.closeHID();
            }
        }

        private void openHID()
        {
            // Current TID and SID are not supported

            if (Delcom.Open() == 0)
            {
                UInt32 FamilyCode, SerialNumber, Version, Date, Month, Year, SecurityNumber, ChipType;
                FamilyCode = SerialNumber = Version = Date = Month = Year = SecurityNumber = ChipType = 0;
                Delcom.GetDeviceInfo(ref FamilyCode, ref SerialNumber, ref Version, ref Date, ref Month, ref Year, ref SecurityNumber, ref ChipType);
                Year += 2000;

                // Del 11-10-2011 - Added test to map LEDs
                if (ChipType == 30 && FamilyCode != 2 && FamilyCode != 3)
                {
                    LED_GREEN_PIN = 0x10;
                    LED_RED_PIN = 0x20;
                    LED_BLUE_PIN = 0x40;
                    // Port0 - All inputs except P0.7 which is the buzzer, Default all high, All Pullups on, All Interrupts Off
                    Delcom.SetupPort(0, 0xFF, 0x7F, 0xFF, 0x00);
                    // Port1 - P1.4-7 Outputs, LED drive pins, Default all high, All Pullups on, All Interrupts Off
                    Delcom.SetupPort(1, 0xFF, 0x0F, 0xFF, 0x00);
                }
                else
                {  // Port1
                    LED_GREEN_PIN = 0x01;
                    LED_RED_PIN = 0x02;
                    LED_BLUE_PIN = 0x04;
                }

                // Optionally -Enable event counter use that auto switch feature work
                TxCmd.MajorCmd = 101;
                TxCmd.MinorCmd = 38;
                TxCmd.LSBData = 1;
                TxCmd.MSBData = 0;
                Delcom.SendCommand(TxCmd);

            }
        }

        private void closeHID()
        {
            if (Delcom != null)
            {
                Delcom.Close();
            }
        }

        private void startActionAfter(TimeSpan timeSpan, Action action)
        {
            Task.Run(() =>
            {
                Thread.Sleep(timeSpan);

                action();
            });
        }

        #region Lights activation

        private Boolean isPresent()
        {
            if (Delcom.IsOpen()) return true;

            return false;
        }

        #region Green

        private void lightGreenOff()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = LED_GREEN_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_GREEN_PIN;
            Delcom.SendCommand(TxCmd);

        }

        private void lightGreenOn()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.MSBData = 0;
            TxCmd.LSBData = LED_GREEN_PIN;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_GREEN_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);
        }

        private void lightGreenFlash()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_GREEN_PIN;
            Delcom.SendCommand(TxCmd);  // enable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_GREEN_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd); // and turn it on
        }

        #endregion Green

        #region Red

        private void lightRedOff()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = LED_RED_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_RED_PIN;
            Delcom.SendCommand(TxCmd);

        }

        private void lightRedOn()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.MSBData = 0;
            TxCmd.LSBData = LED_RED_PIN;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_RED_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);
        }

        private void lightRedFlash()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_RED_PIN;
            Delcom.SendCommand(TxCmd);  // enable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_RED_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd); // and turn it on
        }

        #endregion Green

        #region Yellow

        private void lightYellowOff()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = LED_BLUE_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_BLUE_PIN;
            Delcom.SendCommand(TxCmd);

        }

        private void lightYellowOn()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.MSBData = 0;
            TxCmd.LSBData = LED_BLUE_PIN;
            Delcom.SendCommand(TxCmd);  // always disable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_BLUE_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd);
        }

        private void lightYellowFlash()
        {
            if (!isPresent()) return;
            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 20;
            TxCmd.LSBData = 0;
            TxCmd.MSBData = LED_BLUE_PIN;
            Delcom.SendCommand(TxCmd);  // enable the flash mode 

            TxCmd.MajorCmd = 101;
            TxCmd.MinorCmd = 12;
            TxCmd.LSBData = LED_BLUE_PIN;
            TxCmd.MSBData = 0;
            Delcom.SendCommand(TxCmd); // and turn it on
        }

        #endregion Green

        #endregion Lights activation

        #region IDisposable Implementation

        // Track whether Dispose has been called. 
        private bool disposed = false;

        // Implement IDisposable. 
        // Do not make this method virtual. 
        // A derived class should not be able to override this method. 
        public void Dispose()
        {
            Dispose(true);
            // This object will be cleaned up by the Dispose method. 
            // Therefore, you should call GC.SupressFinalize to 
            // take this object off the finalization queue 
            // and prevent finalization code for this object 
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios. 
        // If disposing equals true, the method has been called directly 
        // or indirectly by a user's code. Managed and unmanaged resources 
        // can be disposed. 
        // If disposing equals false, the method has been called by the 
        // runtime from inside the finalizer and you should not reference 
        // other objects. Only unmanaged resources can be disposed. 
        protected virtual void Dispose(bool disposing)
        {
            // Check to see if Dispose has already been called. 
            if (!this.disposed)
            {
                // If disposing equals true, dispose all managed 
                // and unmanaged resources. 
                if (disposing)
                {
                    // Dispose managed resources.

                    TurnOff();
                    closeHID();
                    Delcom = null;
                }

                // Note disposing has been done.
                disposed = true;

            }
        }

        // Use C# destructor syntax for finalization code. 
        // This destructor will run only if the Dispose method 
        // does not get called. 
        // It gives your base class the opportunity to finalize. 
        // Do not provide destructors in types derived from this class.
        ~DelcomBuildLightDevice()
        {
            // Do not re-create Dispose clean-up code here. 
            // Calling Dispose(false) is optimal in terms of 
            // readability and maintainability.
            Dispose(false);
        }

        #endregion
    }
}
