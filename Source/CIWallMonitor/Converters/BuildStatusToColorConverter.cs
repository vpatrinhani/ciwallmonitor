﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Media;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using CIWallMonitor.Core.Models.CI;

namespace CIWallMonitor.Converters
{
    public class BuildStatusToColorConverter : DependencyObject, IValueConverter
    {
        #region Dependency Properties

        #region SuccessBorderColorBrush

        public Brush SuccessBorderColorBrush
        {
            get { return (Brush)GetValue(SuccessBorderColorBrushProperty); }
            set { SetValue(SuccessBorderColorBrushProperty, value); }
        }

        public static readonly DependencyProperty SuccessBorderColorBrushProperty =
            DependencyProperty.Register("SuccessBorderColorBrush", typeof(Brush), typeof(BuildStatusToColorConverter), new PropertyMetadata(new SolidColorBrush(Colors.DarkGreen)));

        #endregion SuccessBorderColorBrush

        #region SuccessBackgroundColorBrush

        public Brush SuccessBackgroundColorBrush
        {
            get { return (Brush)GetValue(SuccessBackgroundColorBrushProperty); }
            set { SetValue(SuccessBackgroundColorBrushProperty, value); }
        }

        public static readonly DependencyProperty SuccessBackgroundColorBrushProperty =
            DependencyProperty.Register("SuccessBackgroundColorBrush", typeof(Brush), typeof(BuildStatusToColorConverter), new PropertyMetadata(new SolidColorBrush(Colors.LightGreen)));

        #endregion SuccessBackgroundColorBrush

        #region FailureBorderColorBrush

        public Brush FailureBorderColorBrush
        {
            get { return (Brush)GetValue(FailureBorderColorBrushProperty); }
            set { SetValue(FailureBorderColorBrushProperty, value); }
        }

        public static readonly DependencyProperty FailureBorderColorBrushProperty =
            DependencyProperty.Register("FailureBorderColorBrush", typeof(Brush), typeof(BuildStatusToColorConverter), new PropertyMetadata(new SolidColorBrush(Colors.IndianRed)));

        #endregion FailureBorderColorBrush

        #region FailureBackgroundColorBrush

        public Brush FailureBackgroundColorBrush
        {
            get { return (Brush)GetValue(FailureBackgroundColorBrushProperty); }
            set { SetValue(FailureBackgroundColorBrushProperty, value); }
        }

        public static readonly DependencyProperty FailureBackgroundColorBrushProperty =
            DependencyProperty.Register("FailureBackgroundColorBrush", typeof(Brush), typeof(BuildStatusToColorConverter), new PropertyMetadata(new SolidColorBrush(Colors.IndianRed)));

        #endregion FailureBackgroundColorBrush

        #endregion Dependency Properties

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            var _typeOfColor = "background";

            if (parameter != null)
            {
                _typeOfColor = parameter.ToString().ToLower();
            }

            var _buildStatus = (BuildStatus)value;

            switch (_buildStatus)
            {
                case BuildStatus.Success:
                    {
                        if (_typeOfColor == "border")
                        {
                            return this.SuccessBorderColorBrush;
                        }
                        else
                        {
                            return this.SuccessBackgroundColorBrush;
                        }

                        break;
                    }
                case BuildStatus.Failure:
                    {
                        if (_typeOfColor == "border")
                        {
                            return this.FailureBorderColorBrush;
                        }
                        else
                        {
                            return this.FailureBackgroundColorBrush;
                        }

                        break;
                    }
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
