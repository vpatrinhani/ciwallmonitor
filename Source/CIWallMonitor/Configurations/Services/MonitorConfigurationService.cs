﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core.Services;
using CIWallMonitor.Configurations.Models;
using Lex.Db;

namespace CIWallMonitor.Configurations.Services
{
    public class MonitorConfigurationService : LocalPersistenceServiceBase
    {
        #region Constructors

        public MonitorConfigurationService() : 
            base("CIWallMonitorDB")
        {
            
        }

        #endregion Constructors

        #region Methods

        #region Overrides

        protected override void InitializeDBMap()
        {
            base.InitializeDBMap();

            this.DBInstance.Map<MonitorConfigurationModel>()
                .Automap(po => po.Id, true);
        }

        #endregion Overrides

        public MonitorConfigurationModel SaveMonitorConfiguration(MonitorConfigurationModel model)
        {
            this.DBInstance.Save(model);

            return model;
        }

        public MonitorConfigurationModel GetMonitorConfiguration()
        {
            MonitorConfigurationModel _model = null;

            _model = this.DBInstance.LoadAll<MonitorConfigurationModel>().SingleOrDefault();

            if (_model == null)
            {
                _model = this.SaveMonitorConfiguration(new MonitorConfigurationModel());
            }

            return _model;
        }

        #endregion Methods
    }
}
