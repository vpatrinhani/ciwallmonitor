﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core.Models;

namespace CIWallMonitor.Configurations.Models
{
    public class MonitorConfigurationModel : PersistentModelBase
    {
        #region Members

        private string m_ServerAddress = "localhost";
        private int m_AddressPort = 8080;
        private string m_UserName;
        private string m_Password;
        private bool m_UseSSL;
        private bool m_UseBuildLightDevice;

        #endregion Members

        #region Properties

        public string ServerAddress
        {
            get { return m_ServerAddress; }
            set
            {
                m_ServerAddress = value;

                this.RaisePropertyChanged(() => this.ServerAddress);                
            }
        }

        public int AddressPort
        {
            get { return m_AddressPort; }
            set
            {
                m_AddressPort = value;

                this.RaisePropertyChanged(() => this.AddressPort);                
            }
        }

        public string UserName
        {
            get { return m_UserName; }
            set
            {
                m_UserName = value;

                RaisePropertyChanged(() => UserName);
            }
        }

        public string Password
        {
            get { return m_Password; }
            set
            {
                m_Password = value;

                RaisePropertyChanged(() => Password);
            }
        }

        public bool UseSSL
        {
            get { return m_UseSSL; }
            set
            {
                m_UseSSL = value;
                
                RaisePropertyChanged(() => UseSSL);
            }
        }

        public bool UseBuildLightDevice
        {
            get { return m_UseBuildLightDevice; }
            set
            {
                m_UseBuildLightDevice = value;

                RaisePropertyChanged(() => UseBuildLightDevice);
            }
        }

        #endregion Properties
    }
}
