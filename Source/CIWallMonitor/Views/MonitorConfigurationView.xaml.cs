﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CIWallMonitor.ViewModels;

namespace CIWallMonitor.Views
{
    /// <summary>
    /// Interaction logic for MonitorConfigurationView.xaml
    /// </summary>
    public partial class MonitorConfigurationView : UserControl
    {
        private MonitorConfigurationViewModel m_ViewModel;

        public MonitorConfigurationViewModel ViewModel
        {
            get { return m_ViewModel; }
            set
            {
                m_ViewModel = value;

                this.DataContext = m_ViewModel;
            }
        }
        
        public MonitorConfigurationView()
        {
            InitializeComponent();
        }

        private void MonitorConfigurationView_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.LoadMonitorConfiguration();
        }
    }
}
