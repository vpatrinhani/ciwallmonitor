﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CIWallMonitor.ViewModels;

namespace CIWallMonitor.Views
{
    /// <summary>
    /// Interaction logic for BuildWallView.xaml
    /// </summary>
    public partial class BuildWallView
    {
        private BuildWallViewModel m_ViewModel;

        public BuildWallViewModel ViewModel
        {
            get
            {
                if (this.DataContext is BuildWallViewModel)
                {
                    m_ViewModel = (this.DataContext as BuildWallViewModel);
                }

                if (m_ViewModel == null)
                {
                    this.ViewModel = new BuildWallViewModel();
                }

                return m_ViewModel;
            }
            set
            {
                m_ViewModel = value;

                this.DataContext = value;
            }
        }


        public BuildWallView()
        {
            InitializeComponent();

            this.initialize();
        }

        private void initialize()
        {
        }

        private void BuildWallView_OnLoaded(object sender, RoutedEventArgs e)
        {
            this.ViewModel.LoadDataAsync();
        }
    }
}
