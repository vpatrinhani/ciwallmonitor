﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.Internal;
using CIWallMonitor.Configurations.Services;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Services;
using CIWallMonitor.Core.TeamCity;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using Xceed.Wpf.DataGrid.Converters;

namespace CIWallMonitor.Core
{
    public static class IoC
    {
        public const string MonitorConfigurationKey = "MonitorConfiguration";

        public static IUnityContainer Container;

        public static void Initialize()
        {
            Container = ConfigureUnityContainer();

            ConfigureBuildLightDevice();

            ConfigurationMappers.Initialize();
        }

        private static IUnityContainer ConfigureUnityContainer()
        {
            IUnityContainer _container = new UnityContainer();

            _container.RegisterType<MonitorConfigurationService>(
                IoC.MonitorConfigurationKey,
                new PerThreadLifetimeManager()
            );

            _container.RegisterType<IMonitorClient, TeamCityMonitorClient>(
                new ContainerControlledLifetimeManager()
            );

            return _container;
        }

        public static void ConfigureBuildLightDevice()
        {
            var _monitorConfigService = Container.Resolve<MonitorConfigurationService>(IoC.MonitorConfigurationKey);

            var _monitorConfig = _monitorConfigService.GetMonitorConfiguration();

            if (_monitorConfig.UseBuildLightDevice)
            {
                Container.RegisterType<IBuildLightDevice, DelcomBuildLightDevice>(
                    new ContainerControlledLifetimeManager()
                );
            }
            else
            {
                Container.RegisterType<IBuildLightDevice, VoidBuildLightDevice>(
                    new ContainerControlledLifetimeManager()
                );
            }
        }

        public static void ClearMonitorClientInstances()
        {
            Container.RegisterType<IMonitorClient, TeamCityMonitorClient>(
                new ContainerControlledLifetimeManager()
            );
        }
    }
}
