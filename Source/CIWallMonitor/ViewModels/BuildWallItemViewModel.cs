﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Models.CI;
using CIWallMonitor.Core.TeamCity;
using CIWallMonitor.Core.ViewModels;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace CIWallMonitor.ViewModels
{
    public class BuildWallItemViewModel : ViewModelBase
    {
        #region Members

        private Core.Models.CI.BuildTypeModel m_BuildTypeModel;
        private string m_Name;

        #endregion Members

        #region Properties

        private IMonitorClient MonitorClient
        {
            get
            {
                return IoC.Container.Resolve<IMonitorClient>();
            }
        }

        public Core.Models.CI.BuildTypeModel BuildTypeModel
        {
            get { return m_BuildTypeModel; }
            set
            {
                m_BuildTypeModel = value;

                RaisePropertyChanged(() => BuildTypeModel);
            }
        }

        public string Name
        {
            get
            {
                return String.Format("{0} - {1}", this.BuildTypeModel.ProjectName, this.BuildTypeModel.Name);
            }
            private set
            {
                RaisePropertyChanged(() => Name);
            }
        }

        #endregion Properties

        #region Constructors

        public BuildWallItemViewModel()
        {
            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void initialize()
        {
            this.inititalizeMonitorService();
        }

        private void inititalizeMonitorService()
        {
        }

        public void LoadData()
        {

        }

        #endregion Methods
    }
}
