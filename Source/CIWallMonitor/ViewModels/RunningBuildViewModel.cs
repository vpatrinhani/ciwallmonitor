﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core.ViewModels;
using CIWallMonitor.Core.Models.CI;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using AutoMapper;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core;

namespace CIWallMonitor.ViewModels
{
    public class RunningBuildViewModel : ViewModelBase
    {
        #region Members

        private RunningBuildModel m_RunningBuildModel;

        #endregion Members

        #region Properties

        private IMonitorClient MonitorClient
        {
            get
            {
                return IoC.Container.Resolve<IMonitorClient>();
            }
        }

        public RunningBuildModel RunningBuildModel
        {
            get { return m_RunningBuildModel; }
            private set
            {
                m_RunningBuildModel = value;

                this.RaisePropertyChanged(() => this.RunningBuildModel);
            }
        }

        #endregion Properties

        #region Methods

        public async Task UpdateRunningBuildModelAsync(RunningBuildModel runningBuildModel)
        {
            await Task.Run(() =>
            {
                this.UpdateRunningBuildModel(runningBuildModel, true);
            });
        }

        public void UpdateRunningBuildModel(RunningBuildModel runningBuildModel, bool isAsync = false)
        {
            this.SendAsyncUIContext(() =>
            {
                if (this.RunningBuildModel == null)
                {
                    this.RunningBuildModel = this.MonitorClient.ModelFactory.CreateModel<RunningBuildModel>();
                }

                this.RunningBuildModel.CompletedPercent = runningBuildModel.CompletedPercent;
                this.RunningBuildModel.RemainingTime = runningBuildModel.RemainingTime;
                this.RunningBuildModel.ElapsedTime = runningBuildModel.ElapsedTime;
            }, isAsync);

        }

        #endregion Methods
    }
}
