﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Models.CI;
using CIWallMonitor.Core.ViewModels;
using Microsoft.Practices.Unity;
using AutoMapper;

namespace CIWallMonitor.ViewModels
{
    public class BuildViewModel : ViewModelBase
    {
        #region Members

        private BuildModel m_BuildModel;
        private string m_ChangesDescription;
        private RunningBuildViewModel m_RunningBuildViewModel;

        #endregion Members

        #region Properties

        private IMonitorClient MonitorClient
        {
            get
            {
                return IoC.Container.Resolve<IMonitorClient>();
            }
        }

        public BuildModel BuildModel
        {
            get { return m_BuildModel; }
            set
            {
                m_BuildModel = value;

                RaisePropertyChanged(() => BuildModel);

                this.Name = null;
            }
        }

        public string Name
        {
            get
            {
                return String.Format("Build: {0}", this.BuildModel.Number);
            }
            private set
            {
                RaisePropertyChanged(() => Name);
            }
        }

        public string ChangesDescription
        {
            get { return (String.IsNullOrWhiteSpace(m_ChangesDescription)) ? m_ChangesDescription : String.Format("/ {0}", m_ChangesDescription); }
            set
            {
                m_ChangesDescription = value;

                RaisePropertyChanged(() => ChangesDescription);
            }
        }

        public RunningBuildViewModel RunningBuildViewModel
        {
            get { return m_RunningBuildViewModel; }
            private set
            {
                m_RunningBuildViewModel = value;

                this.RaisePropertyChanged(() => this.RunningBuildViewModel);
            }
        }

        #endregion Properties

        #region Constructors
        #endregion Constructors

        #region Methods
        #endregion Methods

        public async Task LoadDataAsync()
        {
            this.IsBusy = true;

            await Task.Run(() =>
            {
                this.LoadData();
            });

            this.IsBusy = false;
        }

        public void LoadData()
        {
            var _changes = this.MonitorClient.GetChangesForBuildID(this.BuildModel.Id);

            if (_changes.Count > 0)
            {
                var _userNameList = new List<string>();

                foreach (var changeModel in _changes)
                {
                    var _changeDetail = this.MonitorClient.GetChange(changeModel.Id);

                    if (!_userNameList.Contains(_changeDetail.UserName))
                    {
                        _userNameList.Add(_changeDetail.UserName);
                    }
                }

                this.ChangesDescription = String.Format("Changes: ({0}) - {1}", _changes.Count, String.Join(", ", _userNameList));
            }
        }

        public async Task UpdateBuildModelAsync(BuildModel buildModel)
        {
            await Task.Run(() =>
            {
                this.UpdateBuildModel(buildModel);
            });
        }

        public void UpdateBuildModel(BuildModel buildModel)
        {
            this.BuildModel = this.MonitorClient.GetBuild(buildModel.Id);

            var _loadDataTask = this.LoadDataAsync();

            _loadDataTask.Wait();
        }

        public async Task UpdateBuildModelFromRunningAsync(RunningBuildModel runningBuildModel)
        {
            await Task.Run(() =>
            {
                this.UpdateBuildModelFromRunning(runningBuildModel, true);
            });
        }

        public void UpdateBuildModelFromRunning(RunningBuildModel runningBuildModel, bool isAsync = false)
        {
            if (runningBuildModel == null)
            {
                this.SendAsyncUIContext(() =>
                {
                    this.RunningBuildViewModel = null;
                }, isAsync);
            }
            else
            {
                this.SendAsyncUIContext(() =>
                {
                    this.RunningBuildViewModel = new RunningBuildViewModel();
                }, isAsync);

                this.RunningBuildViewModel.UpdateRunningBuildModelAsync(runningBuildModel).Wait();

                if (runningBuildModel.BuildId != this.BuildModel.Id)
                {
                    var _buildModel = this.MonitorClient.ModelFactory.CreateModel<BuildModel>();

                    _buildModel.Id = runningBuildModel.BuildId;

                    var _updateBuildModelTask = this.UpdateBuildModelAsync(_buildModel);

                    _updateBuildModelTask.Wait();
                }

                this.BuildModel.StatusText = runningBuildModel.Text;

                this.RunningBuildViewModel.UpdateRunningBuildModelAsync(runningBuildModel).Wait();
            }
        }
    }
}
