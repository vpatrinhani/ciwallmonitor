﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;
using CIWallMonitor.Core;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Models.CI;
using CIWallMonitor.Core.TeamCity;
using CIWallMonitor.Core.ViewModels;
using Microsoft.Practices.ObjectBuilder2;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System.Threading;

namespace CIWallMonitor.ViewModels
{
    public class CurrentBuildChangedEventArgs : EventArgs
    {
        public bool IsRunning { get; set; }
    }

    public class BuildTypeViewModel : ViewModelBase
    {
        #region Event Declarations

        #region CurrentBuildChanged

        public event EventHandler<CurrentBuildChangedEventArgs> CurrentBuildChanged;

        protected void OnCurrentBuildChangedAsync(CurrentBuildChangedEventArgs args)
        {
            Task.Run(() =>
            {
                OnCurrentBuildChanged(args);
            });
        }

        protected void OnCurrentBuildChanged(CurrentBuildChangedEventArgs args)
        {
            if (CurrentBuildChanged != null)
            {
                CurrentBuildChanged(this, args);
            }
        }

        #endregion CurrentBuildChanged

        #endregion Event Declarations

        #region Members

        private Core.Models.CI.BuildTypeModel m_BuildTypeModel;
        private BuildViewModel m_CurrentBuild;
        private ObservableCollection<BuildViewModel> m_LastBuilds;
        private bool m_HasBuildRuninng;
        private Timer m_TimerLoadData;

        #endregion Members

        #region Properties

        private IMonitorClient MonitorClient
        {
            get
            {
                return IoC.Container.Resolve<IMonitorClient>();
            }
        }

        public Core.Models.CI.BuildTypeModel BuildTypeModel
        {
            get { return m_BuildTypeModel; }
            set
            {
                m_BuildTypeModel = value;

                RaisePropertyChanged(() => BuildTypeModel);
            }
        }

        public string Name
        {
            get
            {
                return String.Format("{0} - {1}", this.BuildTypeModel.ProjectName, this.BuildTypeModel.Name);
            }
            private set
            {
                RaisePropertyChanged(() => Name);
            }
        }

        public BuildViewModel CurrentBuild
        {
            get { return m_CurrentBuild; }
            private set
            {
                m_CurrentBuild = value;

                RaisePropertyChanged(() => CurrentBuild);
            }
        }

        public ObservableCollection<BuildViewModel> LastBuilds
        {
            get { return m_LastBuilds; }
            private set { m_LastBuilds = value; }
        }

        public bool HasBuildRuninng
        {
            get { return m_HasBuildRuninng; }
            set
            {
                m_HasBuildRuninng = value;

                RaisePropertyChanged(() => HasBuildRuninng);
            }
        }

        #endregion Properties

        #region Constructors

        public BuildTypeViewModel()
        {
            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void destroyLoadDataTimer()
        {
            if (m_TimerLoadData != null)
            {
                m_TimerLoadData.Dispose();
                m_TimerLoadData = null;
            }
        }

        private void createLoadDataTimer()
        {
            this.destroyLoadDataTimer();

            m_TimerLoadData = new Timer(
                this.timerLoadDataTimerCallback,
                null,
                TimeSpan.FromSeconds(45),
                TimeSpan.FromSeconds(45)
            );
        }

        private void timerLoadDataTimerCallback(object obj)
        {
            if ((m_TimerLoadData == null) || (this.HasBuildRuninng)) return;

            this.LoadDataAsync().Wait();
        }

        private void initialize()
        {
            this.LastBuilds = new ObservableCollection<BuildViewModel>();
        }

        public async Task LoadDataAsync()
        {
            this.IsBusy = true;

            await Task.Run(() =>
            {
                try
                {
                    this.LoadData(true);
                }
                catch (Exception ex)
                {
                    Console.WriteLine();
                }
            });

            this.IsBusy = false;
        }

        public void LoadData(bool isAsync = false)
        {
            this.destroyLoadDataTimer(); 
            
            var _builds = this.MonitorClient.GetBuildsByBuildTypeID(this.BuildTypeModel.Id, 4, true);

            this.SendAsyncUIContext(() =>
            {
                this.CurrentBuild = null;
                this.OnCurrentBuildChanged(new CurrentBuildChangedEventArgs());
                this.LastBuilds.Clear();
            }, isAsync);

            if (_builds.Count > 0)
            {
                var _runingTasks = new List<Task>();

                var _firstBuildModel = _builds.FirstOrDefault();

                if (_firstBuildModel != null)
                {
                    this.SendAsyncUIContext(() =>
                    {
                        this.CurrentBuild = new BuildViewModel()
                        {
                            BuildModel = _firstBuildModel
                        };

                        this.OnCurrentBuildChanged(new CurrentBuildChangedEventArgs());
                    }, isAsync);

                    _runingTasks.Add(this.CurrentBuild.LoadDataAsync());
                }

                _builds.Skip(1).Take(1).ForEach(item =>
                {
                    var _buildViewModel = new BuildViewModel()
                    {
                        BuildModel = item
                    };

                    this.SendAsyncUIContext(() =>
                    {
                        this.LastBuilds.Add(_buildViewModel);
                    }, isAsync);

                    _runingTasks.Add(_buildViewModel.LoadDataAsync());
                });

                Task.WaitAll(_runingTasks.ToArray());
            }

            this.createLoadDataTimer();
        }

        public async Task BuildRunningNotifiedAsync(List<RunningBuildModel> runningBuilds)
        {
            await Task.Run(() =>
            {
                this.BuildRunningNotified(runningBuilds, true);
            });
        }

        public void BuildRunningNotified(List<RunningBuildModel> runningBuilds, bool isAsync = false)
        {
            var _myBuilds = runningBuilds.Where(a => a.BuildTypeId == this.BuildTypeModel.Id).ToList();

            if (_myBuilds.Any())
            {
                this.SendAsyncUIContext(() =>
                {
                    this.HasBuildRuninng = true;
                }, isAsync);

                var _firstBuild = _myBuilds.First();

                this.CurrentBuild.UpdateBuildModelFromRunningAsync(_firstBuild);

                this.OnCurrentBuildChanged(new CurrentBuildChangedEventArgs()
                {
                    IsRunning = true
                });

                var _otherBuilds = _myBuilds.Skip(1).Take(1);

                if (_otherBuilds.Any())
                {
                    //TODO: Tratar mais builds do mesmo tipo.
                }
            }
            else
            {
                this.SendAsyncUIContext(() =>
                {
                    this.HasBuildRuninng = false;
                }, isAsync);

                this.CurrentBuild.UpdateBuildModelFromRunningAsync(null);

                this.OnCurrentBuildChanged(new CurrentBuildChangedEventArgs()
                {
                    IsRunning = false
                });
            }
        }

        #endregion Methods
    }
}
