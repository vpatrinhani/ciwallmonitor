﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using CIWallMonitor.Configurations.Models;
using CIWallMonitor.Configurations.Services;
using CIWallMonitor.Core;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.ViewModels;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace CIWallMonitor.ViewModels
{
    public class MonitorConfigurationViewModel : ViewModelBase
    {
        #region Members

        private MonitorConfigurationModel m_MonitorConfigurationModel;
        private ICommand m_LoadMonitorConfigurationCommand;
        private ICommand m_SaveMonitorConfigurationCommand;

        #endregion Members

        #region Properties

        protected MonitorConfigurationService MonitorConfigurationService
        {
            get { return IoC.Container.Resolve<MonitorConfigurationService>(IoC.MonitorConfigurationKey); }
        }

        public MonitorConfigurationModel MonitorConfigurationModel
        {
            get { return m_MonitorConfigurationModel; }
            private set
            {
                m_MonitorConfigurationModel = value;

                this.RaisePropertyChanged(() => this.MonitorConfigurationModel);
            }
        }

        #region Commands

        public ICommand LoadMonitorConfigurationCommand
        {
            get { return m_LoadMonitorConfigurationCommand; }
            private set
            {
                m_LoadMonitorConfigurationCommand = value;
                
                RaisePropertyChanged(() => LoadMonitorConfigurationCommand);
            }
        }

        public ICommand SaveMonitorConfigurationCommand
        {
            get { return m_SaveMonitorConfigurationCommand; }
            private set
            {
                m_SaveMonitorConfigurationCommand = value;
                
                RaisePropertyChanged(() => SaveMonitorConfigurationCommand);
            }
        }

        #endregion Commands

        #endregion Properties

        #region Constructors

        public MonitorConfigurationViewModel()
        {
            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void initialize()
        {
            this.initializeCommands();
        }

        private void initializeCommands()
        {
            this.LoadMonitorConfigurationCommand = new DelegateCommand(
                this.loadMonitorConfigurationCmdExecute
            );

            this.SaveMonitorConfigurationCommand = new DelegateCommand(
                this.saveMonitorConfigurationCmdExecute
            );
        }

        #region Commands

        private void saveMonitorConfigurationCmdExecute()
        {
            this.SaveMonitorConfiguration();
        }

        private void loadMonitorConfigurationCmdExecute()
        {
            this.LoadMonitorConfiguration();
        }

        public void LoadMonitorConfiguration()
        {
            this.MonitorConfigurationModel = this.MonitorConfigurationService.GetMonitorConfiguration();
        }

        public void SaveMonitorConfiguration()
        {
            this.MonitorConfigurationService.SaveMonitorConfiguration(this.MonitorConfigurationModel);

            IoC.ClearMonitorClientInstances();
        }

        #endregion Commands

        #endregion Methods
    }
}
