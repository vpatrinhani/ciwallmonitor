﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core.TeamCity;
using CIWallMonitor.Core.TeamCity.Models;
using CIWallMonitor.Core.ViewModels;

namespace CIWallMonitor.ViewModels
{
    public class MainWindowViewModel : ViewModelBase
    {
        #region Members

        private BuildWallViewModel m_BuildWallViewModel;

        #endregion Members

        #region Properties

        public BuildWallViewModel BuildWallViewModel
        {
            get { return m_BuildWallViewModel; }
            set
            {
                m_BuildWallViewModel = value;

                RaisePropertyChanged(() => BuildWallViewModel);
            }
        }

        #endregion Properties

        #region Constructors

        public MainWindowViewModel()
        {
        }

        #endregion Constructors
    }
}
