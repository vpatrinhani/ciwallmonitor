﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CIWallMonitor.Core;
using CIWallMonitor.Core.Contracts;
using CIWallMonitor.Core.Models.CI;
using CIWallMonitor.Core.TeamCity;
using CIWallMonitor.Core.ViewModels;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using System.Threading;
using System.Threading.Tasks;

namespace CIWallMonitor.ViewModels
{
    public class BuildWallViewModel : ViewModelBase
    {
        #region Members

        private bool m_BuildRunningStatus = false;
        private BuildStatus m_LastBuildStatus = BuildStatus.None;

        private Timer m_TimerRunningBuilds;
        private ObservableCollection<BuildTypeViewModel> m_Items;

        #endregion Members

        #region Properties

        private IBuildLightDevice BuildLightDevice
        {
            get
            {
                return IoC.Container.Resolve<IBuildLightDevice>();
            }
        }

        private IMonitorClient MonitorClient
        {
            get
            {
                return IoC.Container.Resolve<IMonitorClient>();
            }
        }

        public ObservableCollection<BuildTypeViewModel> Items
        {
            get { return m_Items; }
            private set
            {
                m_Items = value;

                RaisePropertyChanged(() => Items);
            }
        }

        #endregion Properties

        #region Constructors

        public BuildWallViewModel()
        {
            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void initialize()
        {
            this.Items = new ObservableCollection<BuildTypeViewModel>();

            this.inititalizeMonitorService();
        }

        private void inititalizeMonitorService()
        {
        }

        public async Task LoadDataAsync()
        {
            this.IsBusy = true;

            await Task.Run(() =>
            {
                this.LoadData(true);
            });

            this.IsBusy = false;
        }

        public void LoadData(bool isAsync = false)
        {
            this.destroyRunningBuildsTimer();

            this.SendAsyncUIContext(() =>
            {
                foreach (var buildTypeViewModel in m_Items)
                {
                    buildTypeViewModel.CurrentBuildChanged -= _buildItemViewModel_CurrentBuildChanged;
                }

                this.Items.Clear();
            }, isAsync);

            var _buildTypesModels = this.MonitorClient.GetBuildTypes()
                .Where(a => (a.Name != "Code Analysis")).ToList();

            var _runingTasks = new List<Task>();

            foreach (var buildTypeModel in _buildTypesModels)
            {
                var _buildItemViewModel = new BuildTypeViewModel()
                {
                    BuildTypeModel = buildTypeModel
                };

                this.SendAsyncUIContext(() =>
                {
                    this.Items.Add(_buildItemViewModel);

                    _buildItemViewModel.CurrentBuildChanged += _buildItemViewModel_CurrentBuildChanged;
                }, isAsync);

                _runingTasks.Add(_buildItemViewModel.LoadDataAsync());
            }

            Task.WaitAll(_runingTasks.ToArray());

            this.createRunningBuildsTimer();
        }

        private void _buildItemViewModel_CurrentBuildChanged(object sender, CurrentBuildChangedEventArgs e)
        {
            lock (this)
            {
                var _buildTypeViewModel = sender as BuildTypeViewModel;

                if (_buildTypeViewModel != null)
                {
                    if (_buildTypeViewModel.CurrentBuild != null)
                    {
                        if (e.IsRunning)
                        {
                            if (m_BuildRunningStatus != e.IsRunning)
                            {
                                BuildLightDevice.TriggerBuildInProgress();

                                m_BuildRunningStatus = e.IsRunning;
                            }
                        }
                        else
                        {
                            if (m_Items.Any(a => (a.CurrentBuild != null) && (a.CurrentBuild.BuildModel.Status == BuildStatus.Failure)))
                            {
                                if (m_LastBuildStatus != BuildStatus.Failure)
                                {
                                    BuildLightDevice.TriggerBuildBroken();

                                    m_LastBuildStatus = BuildStatus.Failure;
                                }

                                return;
                            }

                            if (m_LastBuildStatus != _buildTypeViewModel.CurrentBuild.BuildModel.Status)
                            {
                                switch (_buildTypeViewModel.CurrentBuild.BuildModel.Status)
                                {
                                    case BuildStatus.None:
                                        BuildLightDevice.TurnOff();
                                        break;
                                    case BuildStatus.Success:
                                        BuildLightDevice.TriggerBuildOK();
                                        break;
                                    case BuildStatus.Failure:
                                        BuildLightDevice.TriggerBuildBroken();
                                        break;
                                }
                            }
                            else
                            {
                                m_LastBuildStatus = _buildTypeViewModel.CurrentBuild.BuildModel.Status;
                            }
                        }
                    }
                }
            }
        }

        private void destroyRunningBuildsTimer()
        {
            if (m_TimerRunningBuilds != null)
            {
                m_TimerRunningBuilds.Dispose();
                m_TimerRunningBuilds = null;
            }
        }

        private void createRunningBuildsTimer()
        {
            this.destroyRunningBuildsTimer();

            m_TimerRunningBuilds = new Timer(
                this.timerRunningBuildsTimerCallback,
                null,
                TimeSpan.FromSeconds(20),
                TimeSpan.FromSeconds(20)
            );
        }

        private void timerRunningBuildsTimerCallback(object obj)
        {
            if (m_TimerRunningBuilds == null) return;

            try
            {
                var _runningBuilds = this.MonitorClient.GetRunningBuilds();

                if (_runningBuilds.Count() <= 0)
                {
                    m_TimerRunningBuilds.Change(TimeSpan.Zero, TimeSpan.FromSeconds(30));
                }
                else
                {
                    m_TimerRunningBuilds.Change(TimeSpan.Zero, TimeSpan.FromSeconds(10));
                }

                this.Items.AsParallel().ForAll(buildType =>
                {
                    buildType.BuildRunningNotifiedAsync(_runningBuilds);
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine();
            }
        }

        #endregion Methods
    }
}
