﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using CIWallMonitor.Core;
using System.Windows.Media;

namespace CIWallMonitor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            GlobalContext.UIAsyncOperation = AsyncOperationManager.CreateOperation(null);

            IoC.Initialize();
        }
    }
}
