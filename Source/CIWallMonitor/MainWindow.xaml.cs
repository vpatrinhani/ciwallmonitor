﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using CIWallMonitor.Core;
using CIWallMonitor.ViewModels;
using CIWallMonitor.Views;
using KeyEventArgs = System.Windows.Input.KeyEventArgs;
using ThicknessConverter = Xceed.Wpf.DataGrid.Converters.ThicknessConverter;

namespace CIWallMonitor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        #region Members

        private bool m_IsInFullscreenMode = false;
        private double m_LastWidth;
        private double m_LastHeight;
        private double m_LastTop;
        private double m_LastLeft;
        private MainWindowViewModel m_ViewModel;

        #endregion Members

        #region Properties

        public MainWindowViewModel ViewModel
        {
            get { return m_ViewModel; }
            private set
            {
                m_ViewModel = value;

                this.DataContext = m_ViewModel;
            }
        }

        #endregion Properties

        #region Constructors

        public MainWindow()
        {
            InitializeComponent();

            this.initialize();
        }

        #endregion Constructors

        #region Methods

        private void initialize()
        {
            this.ViewModel = new MainWindowViewModel();
            
            this.ViewModel.BuildWallViewModel = new BuildWallViewModel();
        }

        private void fullscreenMode()
        {
            this.fullscreenMode(!m_IsInFullscreenMode);
        }

        private void fullscreenMode(bool activate)
        {
            if (!m_IsInFullscreenMode != activate) return;

            if (activate)
            {
                m_LastTop = this.Top;
                m_LastLeft = this.Left;
                m_LastWidth = this.Width;
                m_LastHeight = this.Height;

                this.Topmost = true;
                this.Top = 0;
                this.Left = 0;
                this.WindowState = System.Windows.WindowState.Normal;
                this.ResizeMode = System.Windows.ResizeMode.NoResize;
                this.Width = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width;
                this.Height = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height;
                this.WindowStyle = WindowStyle.None;
                //this.ShowTitleBar = false;
                //this.ShowMaxRestoreButton = false;
                //this.IgnoreTaskbarOnMaximize = true;
                m_IsInFullscreenMode = true;
            }
            else
            {
                this.Topmost = false;
                this.Width = m_LastWidth;
                this.Height = m_LastHeight;
                this.Top = m_LastTop;
                this.Left = m_LastLeft;
                this.WindowState = System.Windows.WindowState.Normal;
                this.ResizeMode = System.Windows.ResizeMode.CanResizeWithGrip;
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                //this.ShowTitleBar = true;
                //this.ShowMaxRestoreButton = true;
                //this.IgnoreTaskbarOnMaximize = false;
                m_IsInFullscreenMode = false;
            }
        }

        private void showOptions()
        {
            var _window = new BaseWindow
            {
                Content = new MonitorConfigurationView()
                {
                    Margin = new Thickness(8),
                    ViewModel = new MonitorConfigurationViewModel(),
                },
                SizeToContent = SizeToContent.WidthAndHeight
            };

            if (m_IsInFullscreenMode)
            {
                _window.Topmost = true;
            }

            _window.Closed += (o, args) =>
            {
                IoC.ConfigureBuildLightDevice();

                this.ViewModel.BuildWallViewModel.LoadDataAsync();
            };

            _window.ShowDialog();
        }

        #endregion Methods

        #region Events

        private void ButtonFullScreenMode_OnClick(object sender, RoutedEventArgs e)
        {
            this.fullscreenMode();
        }
        
        #endregion Events

        private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.fullscreenMode(false);
            }

            if (e.Key == Key.F11)
            {
                this.fullscreenMode(true);
            }

            if ((Keyboard.Modifiers == ModifierKeys.Control) 
                && (e.Key == Key.O))
            {
                this.showOptions();
            }
        }
    }
}
